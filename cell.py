
import bpy

from .primitives import cylinder, sphere
from .materials import make_material

def draw_cell(cell, celllinewidth = 0.05,
    scaled_shift_vector = (0,0,0), shift_vector = (0,0,0),
    name = 'cell', material = None, use_mesh = False, vertex_sphere_size=1.0):

    import numpy as np
    
    if material == None:
        cell_material = make_material(  name = 'cell_material',
                                        color = (0.05, 0.05, 0.05, 1.0))
        # a material being set at all makes it easier to change for all cell cylinders and edges laters
    else:
        cell_material = material
    
    scaled_edge_list = [
        ((0,0,0),(1,0,0)), # x direction
        ((0,0,1),(1,0,1)),
        ((0,1,0),(1,1,0)),
        ((0,1,1),(1,1,1)),
        ((0,0,0),(0,1,0)), # y direction
        ((0,0,1),(0,1,1)),
        ((1,0,0),(1,1,0)),
        ((1,0,1),(1,1,1)),
        ((0,0,0),(0,0,1)), # z direction
        ((0,1,0),(0,1,1)),
        ((1,0,0),(1,0,1)),
        ((1,1,0),(1,1,1))
        ]

    en = 0 
    for scaled_edge in scaled_edge_list:
        location1 = np.dot( np.array(scaled_edge[0]) + np.array(scaled_shift_vector), cell) + \
            np.asarray(shift_vector)
        location2 = np.dot( np.array(scaled_edge[1]) + np.array(scaled_shift_vector), cell) + \
            np.asarray(shift_vector)
        ob = cylinder(
                    location1,
                    location2,
                    radius = celllinewidth,
                    name = '%s_edge_%i'%(name,en),
                    material = cell_material,
                    use_mesh = use_mesh)
        en += 1

    vn = 0 
    for i in range(2):
        for j in range(2):
            for k in range(2):
                center = np.dot( np.array((i,j,k)) + np.array(scaled_shift_vector), cell)+ \
            np.asarray(shift_vector)
                sphere(
                    location = center,
                    radius = celllinewidth*vertex_sphere_size,
                    name = '%s_vertex_%i'%(name,vn),
                    material = cell_material,
                    use_mesh=use_mesh)
                vn +=1

