import bpy

def make_material(name, type='default', **kwargs):
    
    # the default needs to be pretty
    if type=='default':
        type = 'principled'
        if 'roughness' not in kwargs.keys(): # makes sure that you can override it later
            kwargs['roughness'] = 0.02 # gives a beautiful high gloss look, that represent ASE well
    
    full_names = {
    'diffuse':'ShaderNodeBsdfDiffuse',
    'principled':'ShaderNodeBsdfPrincipled',
    'glossy':'ShaderNodeBsdfGlossy',
    'glass':'ShaderNodeBsdfGlass',
    'translucent':'ShaderNodeBsdfTranslucent',
    'transparent':'ShaderNodeBsdfTransparent',
    'refraction':'ShaderNodeBsdfRefraction',
    }
    
    # shaders to add in the future?
    # emission
    # subsurface
    # anisotropic
    
    # name to property index
    kw_diffuse = {
    'color':0,
    'roughness':1}
    
    # subsurface_radius should be a list of 3 values
    kw_principled = {
    'color':0,
    'subsurface':1,
    'subsurface_radius':2,
    'subsurface_color':3,
    'metallic':4,
    'specular':5,
    'specular_tint':6,
    'roughness':7,
    'anisotropic':8,
    'anisotropic_rotation':9,
    'sheen':10,
    'sheen_tint':11,
    'clearcoat':12,
    'clearcoat_roughness':13,
    'ior':14,
    'transmission':15,
    'transmission_roughness':16,
    'emission':17,
    'alpha':18,
    }
    
    kw_glossy = {
    'color':0,
    'roughness':1}
    
    kw_glass = {
    'color':0,
    'roughness':1,
    'ior':2}
    
    kw_refraction = {
    'color':0,
    'roughness':1,
    'ior':2}
    
    kw_translucent = {
    'color':0}
    
    kw_transparent = {
    'color':0}
    
    # gather them to a lookup dict
    input_dicts = {
    'diffuse':kw_diffuse,
    'principled':kw_principled,
    'glossy':kw_glossy,
    'refraction':kw_refraction,
    'glass':kw_glossy,
    'translucent':kw_translucent,
    'transparent':kw_transparent}
    
    
    
    
    assert type.lower() in full_names.keys()
    
    
    mat = bpy.data.materials.new(name=name)
    mat.use_nodes = True
    # clean the node
    nodes = mat.node_tree.nodes
    for node in nodes:
        nodes.remove(node)
    shader_node = nodes.new(type=full_names[type.lower()])
    
    
    ### get the right set of prop to indicies for the material type
    mat_input_dict = input_dicts[type.lower()]
    
    for prop in kwargs.keys():
        index = mat_input_dict[prop]
        prop_value = kwargs[prop]
        shader_node.inputs[index].default_value = prop_value
    
    #shader_node.inputs[0].default_value = color  # color
    #shader_node.inputs[1].default_value = 0.25
    #shader_node.inputs[2].default_value = 1.9 # IOR
    # create output node
    node_output = nodes.new(type='ShaderNodeOutputMaterial')
    links = mat.node_tree.links
    link = links.new(shader_node.outputs[0], node_output.inputs[0])
    
    
    return mat
