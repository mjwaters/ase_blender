def create_atoms_with_skin(atoms, skin_width = 0.5):

    swidth = skin_width/atoms.cell.lengths() # this should use the rcell in the 
    # future to apply a unform skin thickness that isn't subject high angle cells

    supercell = atoms.repeat((3,3,3))
    
    indices_to_delete = []
    sp = supercell.get_scaled_positions()*3 # keep it in multiples of the original cell
    for i in range(len(supercell)):
        delete = False
        for dim in range(3):
            if sp[i,dim] < 1-swidth[dim] or 2+swidth[dim] < sp[i,dim]:
                delete = True
        if delete:
            indices_to_delete.append(i) 
        
    del supercell[indices_to_delete]
    
    supercell.translate(atoms.cell.cartesian_positions([-1,-1,-1]))
    
    return supercell
