import bpy
import numpy as np

def clean():
    for obj in bpy.context.scene.objects:
        #obj.select = True # removed for 2.8
        obj.select_set(True) # added for 2.8
        bpy.ops.object.delete()

def initialize():
    # these need to hooked up
    bpy.data.scenes["Scene"].render.engine = 'CYCLES'
    bpy.data.scenes["Scene"].render.resolution_y = 1080
    bpy.data.scenes["Scene"].render.resolution_x = bpy.data.scenes["Scene"].render.resolution_y #*  (11.81151167/10.22906917)
    bpy.data.scenes["Scene"].render.resolution_percentage = 100
    bpy.data.scenes["Scene"].cycles.samples = 768
    bpy.data.scenes["Scene"].cycles.preview_samples = 32
    #bpy.data.scenes["Scene"].cycles.film_transparent = True # enables background transparency # removed for blender 2.8
    bpy.data.scenes["Scene"].render.film_transparent = True # enables background transparency # added for blender 2.8
    # enables transparent objects to retain alpha instead of just mixing the background color in. Helpful for overlaying pictures later!!
    bpy.data.scenes["Scene"].cycles.film_transparent_glass = True
    bpy.data.scenes["Scene"].cycles.film_transparent_roughness = 0.0
    ############## Set abient/background color

    #bpy.data.worlds["World"].horizon_color = (0.7, 0.7, 0.7) # removed for blender 2.8
    bpy.data.worlds["World"].node_tree.nodes["Background"].inputs[0].default_value = (0.7, 0.7, 0.7, 1.0)     # added for blender 2.8

    ########### performance
    bpy.data.scenes["Scene"].cycles.debug_use_spatial_splits = True

    ######## color processing
    bpy.data.scenes["Scene"].view_settings.view_transform = "Standard"

    #####

    bpy.context.scene.render.image_settings.file_format = 'PNG'
    bpy.context.scene.render.filepath = "ase_render.png"


def render(close_after=False):
    bpy.ops.render.render(write_still = 1)
    ##### system call to make a white backgroun output
    #os.system('convert -flatten test_image.png test_image_na.png')
    if close_after:
        bpy.ops.wm.quit_blender()
        


def complete_vectors(look=None, up=None, right=None):
    ''' creates the camera (or look) basis vectors from user input and
     will autocomplete missing vector non-orthogal vectors using dot products. 
     The look direction will be maintained, up direction has higher priority than right direction'''
    
    # ensure good input
    if type(look)!=type(None):
        assert len(look)==3
        l = np.array(look)
    
    if type(up)!=type(None):
        assert len(up)==3
        u = np.array(up)
    
    if type(right)!=type(None):
        assert len(right)==3
        r = np.array(right)
    
    def normalize(a):
        return np.array(a)/np.linalg.norm(a)
        
    if type(look)!=type(None) and type(up)!=type(None):
        r = normalize(np.cross(l,u))
        u = normalize(np.cross(r,l)) # ensures complete perpendicularity
        l = normalize(np.cross(u,r))
    elif type(look)!=type(None) and type(right)!=type(None):
        u = normalize(np.cross(r,l))
        r = normalize(np.cross(l,u)) # ensures complete perpendicularity
        l = normalize(np.cross(u,r))
    elif type(up)!=type(None) and type(right)!=type(None):
        l = normalize(np.cross(u,r))
        r = normalize(np.cross(l,u)) # ensures complete perpendicularity
        u = normalize(np.cross(r,u))
    else:
        print('some kind of error')
    return l, u, r

def vectors_to_ase_euler_angles(look=[0,0,-1], up=[0,1,0], right=None):
    from scipy.spatial.transform import Rotation as R
    l, u, r = complete_vectors(look=look, up=up, right=right)

    rot = R.from_matrix([r, u, -l])
    # depricated!
    #rot = R.from_dcm([r, u, -l])

    euler_angles = rot.as_euler('xyz', degrees=True)
    euler_string =  "%.6fx, %.6fy, %.6fz" % tuple(euler_angles)
    return euler_angles

def ase_euler_angles_to_vectors(ase_euler_angles):
    from scipy.spatial.transform import Rotation as R

    rot = R.from_euler('xyz',ase_euler_angles, degrees = True)
    mat = rot.as_matrix()
    r = mat[0]
    u = mat[1]
    l = -mat[2]
    return l, u, r

def vectors_to_blender_euler_angles(look=[0,0,-1], up=[0,1,0], right=None, order = 'XYZ'):
    l, u, r = complete_vectors(look=look, up=up, right=right)

    import mathutils 
    mat = mathutils.Matrix()
    mat[0][0:3] = r
    mat[1][0:3] = u
    mat[2][0:3]= -l
    mat.transpose()

    euler_angles = np.array(mat.to_euler(order))*180/np.pi
    euler_string =  "%.6fx, %.6fy, %.6fz" % tuple(euler_angles)
    return euler_angles


def blender_euler_angles_to_vectors(blender_euler_angles, order = 'XYZ'):
    import mathutils 
    import math
    eul = mathutils.Euler(np.array(blender_euler_angles)*np.pi/180, order)

    mat = eul.to_matrix()
    mat.transpose()
    r =  np.array(mat[0][0:3])
    u =  np.array(mat[1][0:3])
    l = -np.array(mat[2][0:3])
    return l, u, r



from .materials import make_material
from .primitives import (sphere, ellipsoid, cylinder, cone, arrow)
from .cell import draw_cell
from .atoms import (draw_atoms, atom_materials_by_element, atom_radii_by_element, create_atoms_with_skin)
from .bonds import (draw_bonds, get_bondpairs, set_high_bondorder_pairs)
from .grids import (draw_isosurface, compute_cutoff_by_percent)
from .camera import draw_camera 
from .polyhedra import draw_polyhedra
