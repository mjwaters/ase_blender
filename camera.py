

import numpy as np
import bpy




def draw_camera(name = 'camera', location=[], blender_euler_angles = [90,0,-90],
    focal_length = 50, clip_distance = 500.0):

    # Create the camera
    #current_layers=bpy.context.scene.layers # removed for blender 2.8
    camera_data = bpy.data.cameras.new(name)
    camera_data.lens = focal_length
    camera_data.clip_end = clip_distance 
    camera = bpy.data.objects.new(name, camera_data)
    camera.location = location
    #camera.layers = current_layers # removed for blender 2.8

    #bpy.context.scene.objects.link(camera) # removed for blender 2.8
    bpy.context.scene.collection.objects.link(camera) # added for blender 2.8

    camera.rotation_euler  = np.pi/180 * np.array(blender_euler_angles)

    #bpy.data.scenes["Scene"].camera = camera # removed for blender 2.8
    bpy.context.scene.camera = camera # added for blender 2.8
    
    return camera
