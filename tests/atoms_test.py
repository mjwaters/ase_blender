import sys
import os
try:
    user_paths = os.environ['PYTHONPATH'].split(os.pathsep)
    sys.path = user_paths + sys.path
except KeyError:
    pass




from ase.spacegroup import crystal


a = b = 3.43
c = 11.64
atoms = crystal(('Ta', 'As'),
                       basis=[(0,0,0.749936), (0.0, 0.0, 0.167764)],
                       spacegroup=109,
                       cellpar=[a, b, c, 90, 90, 90])
                       
                       
#

from ase_blender import draw_atoms

draw_atoms(atoms)
