import sys
import os
try:
    user_paths = os.environ['PYTHONPATH'].split(os.pathsep)
    sys.path = user_paths + sys.path
except KeyError:
    pass




from ase_blender import initialize, clean, draw_camera 
from ase_blender import vectors_to_ase_euler_angles, ase_euler_angles_to_vectors
from ase_blender import vectors_to_blender_euler_angles, blender_euler_angles_to_vectors

clean()
initialize()

from math import sqrt


name =  'camera_11-2'
print('\n',name)
look=[1,1,-2]
up=[1,1,1]
loc = [-2,-2,4]
###########
print('euler angles')
ase_euler_angles =          vectors_to_ase_euler_angles(look=look, up=up)
print('ase    ', ase_euler_angles)
# this tests the backwards conversion which is handy for going from ASE gui to
# blender euler angles
look, up, right = ase_euler_angles_to_vectors(ase_euler_angles)

blender_euler_angles =  vectors_to_blender_euler_angles(look=look, up=up)

print('blender', blender_euler_angles)
draw_camera(name = name, 
    location = loc, 
    blender_euler_angles = blender_euler_angles)

#confirming that we can convert all ways
l, u, r = blender_euler_angles_to_vectors(blender_euler_angles)
print('dir vec, ase, blender')
print('look   ', look,  l)
print('up     ', up,    u)
print('right  ', right, r)




#####################################

name = 'camera_60'
print('\n',name)
look = [1,sqrt(3),0]
up = [0,0,1]
loc = [-2, -2*sqrt(3), 0]
###########
print('euler angles')
ase_euler_angles =          vectors_to_ase_euler_angles(look=look, up=up)
print('ase    ', ase_euler_angles)
# this tests the backwards conversion which is handy for going from ASE gui to
# blender euler angles
look, up, right = ase_euler_angles_to_vectors(ase_euler_angles)

blender_euler_angles =  vectors_to_blender_euler_angles(look=look, up=up)

print('blender', blender_euler_angles)
draw_camera(name = name, 
    location = loc, 
    blender_euler_angles = blender_euler_angles)

#confirming that we can convert all ways
l, u, r = blender_euler_angles_to_vectors(blender_euler_angles)
print('dir vec, ase, blender')
print('look   ', look,  l)
print('up     ', up,    u)
print('right  ', right, r)

###########################


name = 'camera_45'
print('\n',name)
look = [-1,-1,0]
right = [0,0,-1]
loc = [2, 2, 0]
###########
print('euler angles')
ase_euler_angles =          vectors_to_ase_euler_angles(look=look, right=right)
print('ase    ', ase_euler_angles)
# this tests the backwards conversion which is handy for going from ASE gui to
# blender euler angles
look, up, right = ase_euler_angles_to_vectors(ase_euler_angles)

blender_euler_angles =  vectors_to_blender_euler_angles(look=look, right=right)

print('blender', blender_euler_angles)
draw_camera(name = name, 
    location = loc, 
    blender_euler_angles = blender_euler_angles)

#confirming that we can convert all ways
l, u, r = blender_euler_angles_to_vectors(blender_euler_angles)
print('dir vec, ase, blender')
print('look   ', look,  l)
print('up     ', up,    u)
print('right  ', right, r)



# handy for debugging cameras
#for meth in dir(camera):
#    print(meth, getattr(camera,meth))
