import sys
import os
try:
    user_paths = os.environ['PYTHONPATH'].split(os.pathsep)
    sys.path = user_paths + sys.path
except KeyError:
    pass
#####################




from ase_blender import initialize, clean
clean()
initialize()




from ase_blender import sphere, cylinder, cone, arrow, ellipsoid

sphere([1,1,0], 1)

axes = [[1,0,0], [0,2,0], [0,0,3]]
ellipsoid(axes = axes, location = [-5,5,0], )


cylinder([1,1,0], [0,0,3], 1)

cylinder([-0.5,-3,0], [0.5,-3,0.5], 0.5, cap1 = True, cap2 = True)


cone([4,1,2], [3,0,0], 0.5)



## testing cones and materials 
from ase_blender import make_material
blue_material = make_material('blue', color = [0.2,0.2,0.9,1.0])


sphere([0,0,3], 0.9, material = blue_material)
cone([-4,0,0], [-4,0,1], 0.5, material = blue_material, name = 'cone_blue')


red_material = make_material('red', color = [0.9,0.2,0.2,1.0])
cone([-5,0,0], [-5,0,1], 0.5, material = red_material, name = 'cone_red')


## testing arrows 
green_material = make_material('green', color = [0.2,0.9,0.2, 1.0])

arrow([0,4,0], [0,0,4], 1)

import numpy as np 

vector = [1,1,-2]
# scales with with length to keep same aspect ratio
arrow(location = [4,4,0], vector = vector, radius = 0.1*np.linalg.norm(vector),  
    name = 'force_arrow',   material = green_material,
    shaft_radius_fraction = 0.5, head_fraction = 0.3)
