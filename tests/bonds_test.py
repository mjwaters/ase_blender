import sys
import os
try:
    user_paths = os.environ['PYTHONPATH'].split(os.pathsep)
    sys.path = user_paths + sys.path
except KeyError:
    pass





from ase.build import bulk

zb = bulk('ZnS', 'zincblende', a=5.41, cubic=True)

from ase_blender import draw_atoms, draw_bonds

draw_atoms(zb, radius_scale = 0.5)

draw_bonds(zb)
