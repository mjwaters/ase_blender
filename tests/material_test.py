import sys
import os
try:
    user_paths = os.environ['PYTHONPATH'].split(os.pathsep)
    sys.path = user_paths + sys.path
except KeyError:
    pass
#####################



from ase_blender import make_material
test_material = make_material('test')
