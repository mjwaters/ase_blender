import sys
import os
try:
    user_paths = os.environ['PYTHONPATH'].split(os.pathsep)
    sys.path = user_paths + sys.path
except KeyError:
    pass




from ase_blender import initialize, clean
clean()
initialize()




from ase.build import bulk

atoms = bulk('Si', 'diamond', a=5.41, cubic=True)

from ase_blender import draw_atoms, draw_bonds, draw_cell

draw_atoms(atoms, radius_scale = 0.3)
draw_bonds(atoms)
draw_cell(atoms.get_cell())


from ase_blender import draw_isosurface


from ase_blender.grids import create_position_grid, atomic_orbital_real

ngrid = 64

position_grid = create_position_grid(shape = 3*[ngrid], cell = atoms.get_cell())

# returns a 3d numpy array
orbital = atomic_orbital_real(center = atoms.get_positions()[1],
    position_grid = position_grid,
    n = 2, l = 1, m = 0,
    bohr_radius = 0.2) # in Ang for hydrogen, should be ~ 0.53 

orbital_density = orbital.conjugate() * orbital
# always good to check a normalization right? 
print('    norm', orbital_density.sum()*atoms.get_volume()/orbital_density.size )


from ase_blender import make_material
iso_material  = make_material('iso_material',  color = [0.05, 0.35, 0.45, 1.0])


draw_isosurface(orbital_density, atoms.get_cell(),
    name='isosurface',
    contained_percent = 50.0,
    material = iso_material)
