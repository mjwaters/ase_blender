


# modifed from ase.io.pov 
def get_bondpairs(atoms, 
    radius_scale=1.1, radius_dict = {},
    center_indicies = ':', center_elements = None,
    only_include_centers = False):
    
    """Get all pairs of bonding atoms

    Return all pairs of atoms which are closer than radius times the
    sum of their respective covalent radii.  The pairs are returned as
    tuples::

      (a, b, (i1, i2, i3))

    so that atoms a bonds to atom b displaced by the vector::

        _     _     _
      i c + i c + i c ,
       1 1   2 2   3 3

    where c1, c2 and c3 are the unit cell vectors and i1, i2, i3 are
    integers."""

    oic = only_include_centers
    from ase.data import covalent_radii
    from ase.neighborlist import NeighborList
    
    #print(oic)

    cutoffs = []
    for atom in atoms:
        if atom.symbol in radius_dict.keys():
            cutoffs.append( radius_dict[atom.symbol] )
        else:
            cutoffs.append( radius_scale * covalent_radii[atom.number])
    # needs both ways to work on subsets, how can we get around double drawing?
    nl = NeighborList(cutoffs=cutoffs, self_interaction=False, bothways = True ) 
    nl.update(atoms)
    bondpairs = []
    # 3 modes of picking atoms
    if center_indicies != ':': # 1. you pick the center indices directly
        ci = center_indicies
    elif type(center_elements) != type(None):  # 2. you pick the element types 
        ce = set(center_elements)
        ci = []
        for index in range(len(atoms)):
            if atoms[index].symbol in ce:
                ci.append(index)
        #for index in center_indicies:
        #    print(index, atoms[index].symbol)
    else: # 3. everything is included
        #print(center_indicies)
        ci = list(range(len(atoms)))
    
    #print('ci', center_indicies)
    #oic = True
    for a in range(len(atoms)):
        n_indices, offsets = nl.get_neighbors(a)
        #print (n_indices, offsets)
        if a in ci:
            for b, offset in zip(n_indices, offsets):
                if b < a:
                    if oic:
                        if b in ci:
                            #print(b, center_indicies)
                            bondpairs.append((a, b, offset))
                            #print(oic ,atoms[a].symbol, atoms[b].symbol, offset)
                    else:
                        bondpairs.append((a, b, offset))
                        #print(oic ,atoms[a].symbol, atoms[b].symbol, offset)
        else:
            for b, offset in zip(n_indices, offsets):
                if  b < a and b in ci:
                    if oic == False:
                        bondpairs.append((a, b, offset))
                        #print(oic ,atoms[a].symbol, atoms[b].symbol, offset)
                    #else:
                        #if a in center_indicies:
                        #    bondpairs.append((a, b, offset))
                        #    print(atoms[a].symbol, atoms[b].symbol, offset)
                    
                            
    return bondpairs
    
    
from ase.io.pov import  set_high_bondorder_pairs

from ase_blender import cylinder, atom_materials_by_element
import numpy as np

def draw_bonds(atoms, bondpairs = [], bondlinewidth= 0.10,
    atom_materials = [], bond_material = None,
    split_bonds = True, use_caps = True, use_mesh = False):
    '''bond_material takes precedance over atom_materials''' 

    if bondpairs == []:
        bondpairs = get_bondpairs(atoms)

    if bond_material == None:
        if atom_materials == []:
            materials = atom_materials_by_element(atoms)
        else:
            materials = atom_materials
    else:
        materials = [bond_material] * len(atoms)
    
    blw = bondlinewidth 
    for pair in bondpairs:
        # Make sure that each pair has 4 componets: a, b, offset,
        #                                           bond_order, bond_offset
        # a, b: atom index to draw bond
        # offset: original meaning to make offset for mid-point.
        # bond_order: if not supplied, set it to 1 (single bond).
        #            It can be  1, 2, 3, corresponding to single,
        #            double, triple bond
        # bond_offset: displacement from original bond position.
        #              Default is (bondlinewidth, bondlinewidth, 0)
        #              for bond_order > 1.
        if len(pair) == 2:
            a, b = pair
            offset = (0, 0, 0)
            bond_order = 1
            bond_offset = (0, 0, 0)
        elif len(pair) == 3:
            a, b, offset = pair
            bond_order = 1
            bond_offset = (0, 0, 0)
        elif len(pair) == 4:
            a, b, offset, bond_order = pair
            bond_offset = (self.bondlinewidth, self.bondlinewidth, 0)
        elif len(pair) > 4:
            a, b, offset, bond_order, bond_offset = pair
        else:
            raise RuntimeError('Each list in bondatom must have at least '
                               '2 entries. Error at %s' % pair)

        if len(offset) != 3:
            raise ValueError('offset must have 3 elements. '
                             'Error at %s' % pair)
        if len(bond_offset) != 3:
            raise ValueError('bond_offset must have 3 elements. '
                             'Error at %s' % pair)
        if bond_order not in [0, 1, 2, 3]:
            raise ValueError('bond_order must be either 0, 1, 2, or 3. '
                             'Error at %s' % pair)

        # Up to here, we should have all a, b, offset, bond_order,
        # bond_offset for all bonds.

        # Rotate bond_offset so that its direction is 90 degree off the bond
        # Utilize Atoms object to rotate
        if bond_order > 1 and np.linalg.norm(bond_offset) > 1.e-9:
            tmp_atoms = Atoms('H3')
            tmp_atoms.set_cell(self.cell)
            tmp_atoms.set_positions([
                atoms.positions[a],
                atoms.positions[b],
                atoms.positions[b] + np.array(bond_offset),
            ])
            tmp_atoms.center()
            tmp_atoms.set_angle(0, 1, 2, 90)
            bond_offset = tmp_atoms[2].position - tmp_atoms[1].position

        R = np.dot(offset, atoms.get_cell())
        mida = 0.5 * (atoms.positions[a] + atoms.positions[b] + R)
        midb = 0.5 * (atoms.positions[a] + atoms.positions[b] - R)
        if np.any(np.absolute(offset) > 0):
            pbc_bond = True
        else:
            pbc_bond = False
        
        if pbc_bond and use_caps:
            mid_cap = True
        else:
            mid_cap = False

        # draw bond, according to its bond_order.
        # bond_order == 0: No bond is plotted
        # bond_order == 1: use original code
        # bond_order == 2: draw two bonds, one is shifted by bond_offset/2,
        #                  and another is shifted by -bond_offset/2.
        # bond_order == 3: draw two bonds, one is shifted by bond_offset,
        #                  and one is shifted by -bond_offset, and the
        #                  other has no shift.
        # To shift the bond, add the shift to the first two coordinate in
        # write statement.
        if split_bonds:
            if bond_order == 1:
                cylinder(atoms.positions[a], mida, blw, 
                        name = 'bond_%s%i_%s%i'%(atoms[a].symbol, a , atoms[b].symbol, b), 
                        material = materials[a], cap2 = mid_cap, use_mesh = use_mesh )
                cylinder(atoms.positions[b], midb, blw,
                        name = 'bond_%s%i_%s%i'%(atoms[b].symbol, b , atoms[a].symbol, a),
                        material = materials[b], cap2 = mid_cap, use_mesh = use_mesh )
            elif bond_order == 2:
                bondOffSetDB = [x / 2 for x in bond_offset]
                
                cylinder(atoms.positions[a]-bondOffSetDB, mida-bondOffSetDB, blw, 
                        name = 'bond_%s%i_%s%i_0'%(atoms[a].symbol, a , atoms[b].symbol, b), 
                        material = materials[a], cap2 = mid_cap, use_mesh = use_mesh )
                cylinder(atoms.positions[a]+bondOffSetDB, mida+bondOffSetDB, blw,
                        name = 'bond_%s%i_%s%i_1'%(atoms[a].symbol, a , atoms[b].symbol, b), 
                        material = materials[a], cap2 = mid_cap, use_mesh = use_mesh )
                        
                cylinder(atoms.positions[b]-bondOffSetDB, midb-bondOffSetDB, blw,
                        name = 'bond_%s%i_%s%i_0'%(atoms[b].symbol, b , atoms[a].symbol, a), 
                        material = materials[b], cap2 = mid_cap, use_mesh = use_mesh )
                cylinder(atoms.positions[a]+bondOffSetDB, mida+bondOffSetDB, blw,
                        name = 'bond_%s%i_%s%i_1'%(atoms[b].symbol, b , atoms[a].symbol, a), 
                        material = materials[b], cap2 = mid_cap, use_mesh = use_mesh )

            elif bond_order == 3:
                cylinder(atoms.positions[a], mida, blw,
                        name = 'bond_%s%i_%s%i_0'%(atoms[a].symbol, a , atoms[b].symbol, b), 
                        material = materials[a], cap2 = mid_cap, use_mesh = use_mesh )
                cylinder(atoms.positions[a]-bond_offset, mida-bond_offset, blw,
                        name = 'bond_%s%i_%s%i_1'%(atoms[a].symbol, a , atoms[b].symbol, b), 
                        material = materials[a], cap2 = mid_cap, use_mesh = use_mesh  )
                cylinder(atoms.positions[a]+bond_offset, mida+bond_offset, blw,
                        name = 'bond_%s%i_%s%i_2'%(atoms[a].symbol, a , atoms[b].symbol, b), 
                        material = materials[a], cap2 = mid_cap, use_mesh = use_mesh  )
                    
                cylinder(atoms.positions[b], midb, blw,
                        name = 'bond_%s%i_%s%i_0'%(atoms[b].symbol, b , atoms[a].symbol, a), 
                        material = materials[b], cap2 = mid_cap, use_mesh = use_mesh  )
                cylinder(atoms.positions[b]-bond_offset, midb-bond_offset, blw,
                        name = 'bond_%s%i_%s%i_1'%(atoms[b].symbol, b , atoms[a].symbol, a), 
                        material = materials[b], cap2 = mid_cap, use_mesh = use_mesh  )
                cylinder(atoms.positions[a]+bond_offset, mida+bond_offset, blw,
                        name = 'bond_%s%i_%s%i_2'%(atoms[b].symbol, b , atoms[a].symbol, a), 
                        material = materials[b], cap2 = mid_cap, use_mesh = use_mesh  )
        else:
            # unsplit bonds
            print('unsplit bonds are not yet implemented')
