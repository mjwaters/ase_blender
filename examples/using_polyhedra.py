import sys
import os
try:
    user_paths = os.environ['PYTHONPATH'].split(os.pathsep)
    sys.path = user_paths + sys.path
except KeyError:
    pass


from ase import Atoms
import numpy as np
sym = [ 'Ca','Ca','Ca','Ca',
        'Mn','Mn','Mn','Mn',
        'O', 'O', 'O', 'O',
        'O', 'O', 'O', 'O',
        'O', 'O', 'O', 'O']
pos = [
        (0.991521,  0.044799,  0.750000),
        (0.491521,  0.455201,  0.250000),
        (0.508479,  0.544799,  0.750000),
        (0.008479,  0.955201,  0.250000),
        (0.500000,  0.000000,  0.500000),
        (0.000000,  0.500000,  0.500000),
        (0.000000,  0.500000,  0.000000),
        (0.500000,  0.000000,  0.000000),
        (0.921935,  0.520580,  0.250000),
        (0.421935,  0.979420,  0.750000),
        (0.578065,  0.020580,  0.250000),
        (0.078065,  0.479420,  0.750000),
        (0.707456,  0.291917,  0.959281),
        (0.207456,  0.208083,  0.040719),
        (0.792544,  0.791917,  0.540719),
        (0.292544,  0.708083,  0.459281),
        (0.707456,  0.291917,  0.540719),
        (0.207456,  0.208083,  0.459281),
        (0.292544,  0.708083,  0.040719),
        (0.792544,  0.791917,  0.959281)]
cellpar=[5.40, 5.51,  7.70, 90, 90, 90]
uc = Atoms(symbols = sym, scaled_positions = pos, cell = cellpar)
uc.set_pbc( True)
sp = uc.get_scaled_positions() 
uc.set_scaled_positions(sp - np.array([0.25,0.25,0.25]) )
uc.wrap()
atoms = uc.repeat((2,2,1))

sym = atoms.get_chemical_symbols()
sym[26] = 'Fe'
sym[44] = 'Ti'
atoms.set_chemical_symbols(sym)

from ase_blender import initialize, clean
clean()
initialize()


from ase_blender import draw_polyhedra
draw_polyhedra(atoms, center_elements = ['Ti','Mn','Fe'], vertex_elements = ['O'])

from ase_blender import draw_atoms
draw_atoms(atoms, radius_scale = 0.4)


from ase_blender import draw_cell
draw_cell(atoms.cell)


from ase_blender import draw_bonds, get_bondpairs
from ase_blender import make_material
oct_material  = make_material('oct_material',  color = [0.05, 0.25, 0.45, 1.0])
bp_oct = get_bondpairs(atoms, center_elements = ['Ti','Mn','Fe'], radius_dict = {'Ca': 0.0}, radius_scale = 0.9)
draw_bonds(atoms, bondpairs = bp_oct, bond_material = oct_material )
