

import bpy

from ase_blender import atom_materials_by_element
import numpy as np
from scipy.spatial import ConvexHull

def draw_polyhedra(atoms,  
    atom_materials = [], polyhedron_material = None,
    radius_scale=1.1, radius_dict = {},
    center_indicies = ':', vertex_indicies = ':', 
    center_elements = None, vertex_elements = None):


    if polyhedron_material == None:
        if atom_materials == []:
            materials = atom_materials_by_element(atoms, prefix = 'poly_', type = 'default', alpha = 0.4)
        else:
            materials = atom_materials
    else:
        materials = [polyhedron_material] * len(atoms)



    #### compute cutoffs
    from ase.data import covalent_radii
    cutoffs = []
    for atom in atoms:
        if atom.symbol in radius_dict.keys():
            cutoffs.append( radius_dict[atom.symbol] )
        else:
            cutoffs.append( radius_scale * covalent_radii[atom.number])
    
    #### needs both ways to work on subsets
    from ase.neighborlist import NeighborList
    nl = NeighborList(cutoffs=cutoffs, self_interaction=False, bothways = True ) 
    nl.update(atoms)
  
    # 3 modes of picking atoms
    if center_indicies != ':': # 1. you pick the center indices directly
        ci = center_indicies
    elif type(center_elements) != type(None):  # 2. you pick the element types 
        ce = set(center_elements)
        ci = []
        for index in range(len(atoms)):
            if atoms[index].symbol in ce:
                ci.append(index)
    else: # 3. everything is included
        ci = list(range(len(atoms)))
        
        
    ### do the same for vertices
    if vertex_indicies != ':': 
        vi = vertex_indicies
    elif type(vertex_elements) != type(None):  
        ce = set(vertex_elements)
        vi = []
        for index in range(len(atoms)):
            if atoms[index].symbol in ce:
                vi.append(index)
    else:
        vi = list(range(len(atoms)))



    # loop over center atoms and make polyhedra
    from ase.geometry import find_mic
    for a in ci:
        n_indices, offsets = nl.get_neighbors(a)
        filtered_neighbors = []
        for b, offset in zip(n_indices, offsets):
            if b in vi:
                filtered_neighbors.append(b)

        if True: filtered_neighbors.append(a)

        #print(a, filtered_neighbors)
        if len(filtered_neighbors)>=4:
            D_array = np.zeros((len(filtered_neighbors), 3))
            for index in range(len(filtered_neighbors)):
                neighbor_index = filtered_neighbors[index]
                D_array[index] = atoms.positions[neighbor_index] - atoms.positions[a]
            relative_positions, dist =  find_mic(D_array, atoms.cell) #pbc = True

            hull = ConvexHull(relative_positions)
            hull_centroid = np.mean(hull.points[hull.vertices],axis=0)

            # centroid is *always* inside the convexhull
            #print(hull_centroid)


            list_tuples_for_blender = []
            for polycorners in hull.simplices:
                vec_to_first_vertex = hull.points[polycorners[0]] - hull_centroid
                vec_0_to_1          = hull.points[polycorners[1]] - hull.points[polycorners[0]]
                vec_1_to_2          = hull.points[polycorners[2]] - hull.points[polycorners[1]]
                normal_projection = vec_to_first_vertex.dot( np.cross (vec_0_to_1 , vec_1_to_2  ))

                if normal_projection >= 0:
                    list_tuples_for_blender.append(tuple(polycorners))
                else:
                    pc_fixed = (polycorners[0], polycorners[2], polycorners[1])
                    list_tuples_for_blender.append(pc_fixed)


            output_positions = np.add(hull.points, atoms.positions[a])

            name = atoms[a].symbol + str(a) + '_polyhedron'
            mesh = bpy.data.meshes.new(name)
            mesh.from_pydata(output_positions, [], list_tuples_for_blender)

            ob = bpy.data.objects.new(name,mesh)
            ob.data.materials.append(materials[a]) 
            #bpy.context.scene.objects.link(ob) ## removed for blender 2.8
            bpy.context.scene.collection.objects.link(ob) # added for blender 2.8


if False:
# it might be nice to have something like this in the future
    def polyhedron( name = None, material = None ):



        # Create Mesh Datablock
        mesh = bpy.data.meshes.new(name)
        mesh.from_pydata(output_positions, [], list_tuples_for_blender)

        obj = bpy.data.objects.new(name,mesh)
        bpy.context.scene.objects.link(obj)


        ob = bpy.context.object

        if name != None:
            ob.name = name
        if material != None:
            ob.data.materials.append(material)





