


def draw_atom_vectors(atoms, vectors, scale = 1.0, 
    radius = 0.4, radius_function = None,
    name = 'vector', material = None, 
     **arrow_kwargs):


    # todo:
    # need to test if material is iterable, 
    # if so, check the lengths to see if it matches
    
    # todo: 
    # maybe set a minimum size vector to draw?


    def constant_radius_function(length):
        return radius

    if callable(radius_function):
        rf = radius_function
    else:
        rf = constant_radius_function

    
    #  will work in all dimensions!
    assert atoms.get_positions().shape == np.asarray(vectors).shape
    
    positions =  atoms.get_positions()
    
    for atom_index in range(len(atoms)):
        
        arrow_name = name + '_{:d}'.format(atom_index)
        
        length = np.linalg.norm(vectors[atom_index])
        vector_radius = rf(length)
        
        arrow(  location = positions[atom_index],
                vector = np.array(vectors[atom_index]) *scale,
                radius = vector_radius,
                name = arrow_name,
                material = material,
                **arrow_kwargs)
