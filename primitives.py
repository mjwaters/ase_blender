import bpy
import numpy as np



def cylinder(location1, location2, radius,  name = None, material = None,
    cap1 = False, cap2 = False, 
    resolution_u = 12, resolution_v = 4, 
    use_mesh = False, vertices = 72,
    smooth_shading = True):


    #end caps need more work

    end1 = np.array(location1)
    end2 = np.array(location2)
    location_cyl = (end1+end2)*0.5

    delta = end2-end1
    dx = delta[0]
    dy = delta[1]
    dz = delta[2]

    length_cyl = np.sqrt( dx**2 + dy**2 + dz**2  )

    theta1 = 0.0
    theta2 = np.pi/2.0 - np.arcsin(dz/length_cyl)
    theta3 = np.arctan2(dy,dx)

    if use_mesh==False:
        bpy.ops.surface.primitive_nurbs_surface_cylinder_add(
            radius = 1.0,
            location=location_cyl)

        # resize then rotate prevents ellipsoidal cross sections
        bpy.ops.transform.resize(value=(radius, radius, length_cyl/2.0))

        ob = bpy.context.object
        #convert the orientation to a euler so we can easily set it#
        myOrient = ob.matrix_world.to_euler()
        #set the new orientation (in radians)#
        myOrient[0] = x_orientation = theta1
        myOrient[1] = y_orientation = theta2
        myOrient[2] = z_orientation = theta3

        ob.rotation_euler = myOrient
        
        if name != None:
            ob.name = name
        if material != None:
            ob.data.materials.append(material)
        
        ob.data.resolution_u = resolution_u
        ob.data.resolution_v = resolution_v
        
        if cap1:
            # It would be nice to use a smooth circle
            #bpy.ops.surface.primitive_nurbs_surface_circle_add(
            bpy.ops.mesh.primitive_circle_add(vertices=vertices, fill_type='NGON',
                radius = radius,
                location = end1,
                rotation = (theta1+np.pi,theta2,theta3))
            ob_cap1 = bpy.context.object
            if ob.name != None: ob_cap1.name =ob.name+"_cap1"
            if material != None: ob_cap1.data.materials.append(material)

        if cap2:
            # It would be nice to use a smooth circle
            #bpy.ops.surface.primitive_nurbs_surface_circle_add(
            bpy.ops.mesh.primitive_circle_add(vertices=vertices, fill_type='NGON',
                radius = radius,
                location = end2,
                rotation = (theta1,theta2,theta3))
            ob_cap1 = bpy.context.object
            if ob.name != None: ob_cap1.name = ob.name+"_cap2"
            if material != None: ob_cap1.data.materials.append(material)
    else:
        # it's easier to setup a cylinder mesh than nurbs
        bpy.ops.mesh.primitive_cylinder_add(
            vertices = vertices,
            radius = radius,
            depth = length_cyl,
            end_fill_type = 'NGON',
            location=location_cyl,
            rotation=(theta1, theta2, theta3))
            
        ob = bpy.context.object

        if smooth_shading:
            # use object name to get the mesh
            # bpy.data.meshes[ob.name].use_auto_smooth = True
            ob.data.use_auto_smooth = True
            ob.select_set(True) # added for 2.8
            bpy.ops.object.shade_smooth()
        
        if name != None:
            ob.name = name
        if material != None:
            ob.data.materials.append(material)
        

    return ob


def sphere(location, radius=1, name = None, material = None,
    resolution_u = 24, resolution_v = 8,
    use_mesh=False, subdivisions=4, smooth_shading=True):
    
    if use_mesh:
        bpy.ops.mesh.primitive_ico_sphere_add(subdivisions=4, radius = radius, location=location)
        ob = bpy.context.object
        if smooth_shading:
            # use object name to get the mesh
            # bpy.data.meshes[ob.name].use_auto_smooth = True
            ob.data.use_auto_smooth = True
            ob.select_set(True) # added for 2.8
            bpy.ops.object.shade_smooth()
    
    else:
        bpy.ops.surface.primitive_nurbs_surface_sphere_add( location=location, radius = radius)   
        ob = bpy.context.object

        ob.data.resolution_u = resolution_u
        ob.data.resolution_v = resolution_v
        
    if name != None:
        ob.name = name
    if material != None:
        ob.data.materials.append(material)
        
    return ob
    


def ellipsoid(axes, **sphere_kwargs):
    
    ob = sphere(**sphere_kwargs)
    
    mat4 = np.zeros((4,4))
    mat4[0:3,0:3] = axes
    ob.data.transform(mat4)
    
    return ob



def cone(base, tip, radius,  name = None, material = None, smooth_shading = True, vertices = 72):
        
    # smooth shading makes the polyhedral cone look like a smooth cone
    # if you are close to the cone and still see polygons, increase vertices

    end1 = np.array(base)
    end2 = np.array(tip)
    location = (end1+end2)*0.5

    delta = end2-end1
    dx = delta[0]
    dy = delta[1]
    dz = delta[2]

    depth = np.sqrt( dx**2 + dy**2 + dz**2  )

    theta1 = 0.0
    theta2 = np.pi/2.0 - np.arcsin(dz/depth)
    theta3 = np.arctan2(dy,dx)

    rotation = (theta1,theta2,theta3)


    bpy.ops.mesh.primitive_cone_add(vertices=vertices, radius1=radius,
        depth=depth, location=location, rotation=rotation)

    # get the object
    ob = bpy.context.object

    #for meth in dir(ob.data):
    #    print(meth, getattr(ob.data,meth))

    if name != None:
        ob.name = name
        # this reduces confusion when selecting meshes
        ob.data.name = name
    if material != None:
        ob.data.materials.append(material)    

    if smooth_shading:
        # use object name to get the mesh
       # bpy.data.meshes[ob.name].use_auto_smooth = True
        ob.data.use_auto_smooth = True
        ob.select_set(True) # added for 2.8
        bpy.ops.object.shade_smooth()

    ## link the cone
    bpy.context.scene.collection.objects.link(ob) 

        
    return ob




def arrow(location, vector, radius,  name = None, material = None, 
    attach_point = 'tail' , shaft_radius_fraction = 0.5, head_fraction = 0.3,
    cap = True, vertices = 72): 
    
    #attach_point = 'tail', 'head', or float
    if attach_point.lower() ==  'tail':
        ap = 0.0
    elif attach_point.lower() == 'head': 
        ap = 1.0
    else:
        ap = float(attach_point)
        
    
    shaft_tail  = np.asarray(location)-ap*np.asarray(vector)
    head_tip  = shaft_tail + np.asarray(vector)
    head_base = shaft_tail + (1.0-head_fraction) * np.asarray(vector)
    
    
    if name == None:
        shaft_name  = 'shaft'
        head_name   = 'head'
    else:
        shaft_name  = name+'_shaft'
        head_name   = name+'_head'

    cylinder(location1 = shaft_tail, location2 = head_base, radius = shaft_radius_fraction*radius,  
        name = shaft_name, material = material,
        cap1 = cap, cap2 = False, vertices = vertices)
    
    cone(base = head_base, tip = head_tip, radius = radius,  
        name = head_name, material = material,
        smooth_shading = True, vertices = vertices)
        
