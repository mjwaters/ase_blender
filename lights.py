



################## Create a lamp
#current_layers=bpy.context.scene.layers # removed for blender 2.8
#lamp_data = bpy.data.lamps.new(name="A_lamp", type="AREA")  # removed for blender 2.8
lamp_data = bpy.data.lights.new(name="A_lamp", type="AREA")  # added for blender 2.8
lamp_data.energy = 7001
#lamp_data.distance = 500.0
lamp_data.size = 15.0
#lamp_data.shadow_method = 'RAY_SHADOW'
lamp_data.use_nodes = True
lamp = bpy.data.objects.new("A_lamp", lamp_data)


# we can set it relative to the cell
#lamp.location = array([0.0, 1.0, 0.0]).dot(cell) + array([0,0,10.0])
lamp.location =  [3,10.0,10.0]

#lamp.layers = current_layers ## removed for blender 2.8
#lamp.rotation_euler  = Euler([0, 0, 45*pi/180])
lamp.rotation_euler  = Euler([-30 * pi/180, 0, 0])

#bpy.context.scene.objects.link(lamp) # removed for blender 2.8
bpy.context.scene.collection.objects.link(lamp) # added for blender 2.8
#lmp_object = bpy.data.lamps["A_lamp"]  # removed for blender 2.8
lmp_object = bpy.data.lights["A_lamp"] # added for blender 2.8

#lmp_object.node_tree.nodes['Emission'].inputs['Strength'].default_value = 6001 # switch to new property for 2.8 -> lamp_data.energy

