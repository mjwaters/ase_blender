


from ase.data.colors import jmol_colors as default_colors

from ase.data import chemical_symbols, atomic_numbers
from .materials import make_material


def atom_materials_by_element(atoms, prefix = '', type ='default', 
        element_color_dict = None,
        element_material_dict = None, 
        **mat_kwargs):
        
    if element_color_dict is None:
        colors = default_colors
    else:
        colors = default_colors.copy()
        for el, el_color in element_color_dict.items():
            z = atomic_numbers[el]
            colors[z] = el_color
        
        
    if element_material_dict is None:
        element_material_dict = {} # creating an empty set to make testing easier


    zs = atoms.get_atomic_numbers()
    unique_zs = list(set(zs))
    z_material_dict = {}
    for unique_z in unique_zs:
        unique_el = chemical_symbols[unique_z]
        
        if unique_el in element_material_dict.keys(): 
            z_material_dict[unique_z] = element_material_dict[unique_el]
            
        else:
            c = list(colors[unique_z]) +[1.0] # blender uses RGBA colors
            material = make_material(  name = prefix+chemical_symbols[unique_z],
                                type=type, color =  c, **mat_kwargs)
            z_material_dict[unique_z] = material
    
    atom_materials = []
    for z in zs:
        atom_materials.append(z_material_dict[z])
        
    return atom_materials



from ase.data import covalent_radii
def atom_radii_by_element(atoms, element_radius_dict = None, radius_scale = 1.0 ):
    if element_radius_dict is None:
        element_radius_dict = {}
    
    radii = []
    for atom in atoms:
        if atom.symbol in element_radius_dict.keys():
            radii.append( element_radius_dict[atom.symbol])
        else:
            radii.append( radius_scale * covalent_radii[atom.number])

    return radii


from ase_blender import sphere

def draw_atoms(atoms, radius_scale = 1.0, atom_radii = [], atom_materials = [], use_mesh=False):
        
    if atom_radii == []:
        radii = [ radius_scale * covalent_radii[atom.number] for atom in atoms ]
    else:
        radii = atom_radii


    if atom_materials == []:
        materials = atom_materials_by_element(atoms)
    else:
        materials = atom_materials
        
    positions = atoms.get_positions()
    
    for i in range(len(atoms)):
        name = atoms[i].symbol+"_%i"%i
        
        sphere(location = positions[i], 
            radius = radii[i], 
            name = name, 
            material = materials[i],
            use_mesh=use_mesh)
            
            
            
def create_atoms_with_skin(atoms, skin_width = 0.5):
    #cell0 = atoms.get_cell()
    swidth = skin_width/atoms.cell.lengths() # this should use the rcell in the 
    # future to apply a unform skin thickness that isn't subject high angle cells

    supercell = atoms.repeat((3,3,3))
    indices_to_delete = []
    sp = supercell.get_scaled_positions()*3 # keep it in multiples of the original cell
    for i in range(len(supercell)):
        delete = False
        for dim in range(3):
            if sp[i,dim] < 1-swidth[dim] or 2+swidth[dim] < sp[i,dim]:
                delete = True
        if delete:
            indices_to_delete.append(i) 
    del supercell[indices_to_delete]
    
    supercell.translate(atoms.cell.cartesian_positions([-1,-1,-1]))
    #supercell.set_cell(cell0) # wont work with draw bonds
    return supercell
