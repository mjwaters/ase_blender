import bpy
import numpy as np

from . import compute_cutoff_by_percent
from .. import make_material

from skimage.measure import  marching_cubes  #marching_cubes_lewiner is deprecated
    

def draw_isosurface(density_grid, cell, name='isosurface',
    cut_off = None, contained_percent = None,
    material = None,
    super_cell = (1,1,1),
    oversample = 1, smooth_shading = True,
    closed_edges = False, gradient_ascending = False,
    shift_vector = (0,0,0),        wrap_vector = (0,0,0),
    scaled_shift_vector = (0,0,0), scaled_wrap_vector = (0,0,0),
    coarse_wrapping = True,
    verbose = False):


    """
    rho: a 3D numpy array as data grid, indexed [x,y,z]
    cell: an ASE cell object
    """

    rho = np.copy(density_grid)

    if cut_off == None:
        if contained_percent == None:
            cut_off = (rho.max() - rho.min())/2.0
            print('cut_off ', cut_off)
        else:
            cut_off = compute_cutoff_by_percent(rho, percent = contained_percent)
            print('cut_off from contained_percent:', cut_off)





    # combine scaled_wrap_vector and wrap_vector into one scaled_wrap_vector_combined (swvc)
    swvc = np.array(scaled_wrap_vector) + np.dot(wrap_vector, np.linalg.inv(cell).T)
    if coarse_wrapping == True:
        int_wrap = (swvc * np.array(rho.shape)).astype(int)
        rho = np.roll(rho, shift = int_wrap , axis = (0,1,2) )
    else:
        #fourier interpolation for sub grid shifts
        n = rho.shape
        k_rho = np.fft.fftn(rho)
        gshift =  swvc*np.array(rho.shape) # grid shift
        kx, ky, kz = np.meshgrid(np.fft.fftfreq(n[0]), np.fft.fftfreq(n[1]), np.fft.fftfreq(n[2]), indexing = 'ij' )
        phase_shift = np.exp(-2.0j*np.pi * ( kx*gshift[0] + ky*gshift[1] + kz*gshift[2]  ))
        rho = np.fft.ifftn(k_rho*phase_shift).real



    combined_shift_vector = np.array(shift_vector) + np.dot(scaled_shift_vector, cell)





    if False: # this would be nice if this oversample worked
        new_dimensions = ( oversample_factor* np.array(rho.shape) ).astype(int)
        print (rho.shape, 'Sampled to', new_dimensions)
        #k_rho = np.fft.fftshift( np.fft.fftn(rho, s = new_dimensions) )
        k_rho =  np.fft.fftn(rho, s = new_dimensions )

        #k_rho_oversample = np.fft.ifftshift(k_rho_oversample)
        rho = np.fft.ifftn(k_rho)
        rho = ((1.0*rho.size)/k_rho.size)*rho.real


    if oversample!=1:
        #from numpy import fft
        k_rho = np.fft.fftshift( np.fft.fftn(rho))
        new_dimensions = ( oversample* np.array(k_rho.shape) ).astype(int)
        #new_dimensions = (1024,1024,1024)
        print (k_rho.shape, 'Sampled to', new_dimensions)
        k_rho_oversample = np.zeros(new_dimensions, dtype = complex)
        lb = [0,0,0]
        ub = [0,0,0]
        if oversample >= 1.0:
            for i in range(3):
                lb[i] = k_rho_oversample.shape[i]//2 - k_rho.shape[i]//2
                ub[i] = lb[i] + k_rho.shape[i]
            k_rho_oversample[lb[0]:ub[0], lb[1]:ub[1], lb[2]:ub[2]] = k_rho
        else:
            for i in range(3):
                lb[i] = k_rho.shape[i]//2 - k_rho_oversample.shape[i]//2
                ub[i] = lb[i] + k_rho_oversample.shape[i]
            k_rho_oversample = k_rho[lb[0]:ub[0], lb[1]:ub[1], lb[2]:ub[2]]
        k_rho_oversample = np.fft.ifftshift(k_rho_oversample)
        rho = np.fft.ifftn(k_rho_oversample)
        rho = ((1.0*rho.size)/k_rho.size)*rho.real



    if super_cell!=(1,1,1):
        #from numpy import tile
        rho = np.tile(rho, super_cell)
        old_cell = cell
        cell = np.array([[super_cell[0], 0, 0],
                        [0,  super_cell[1], 0],
                        [0,  0,  super_cell[2]]
                        ]).dot(old_cell)
        print('Cell', old_cell)
        print('Expanded to', cell)

    #########
    gradient_direction = 'descent'

    if closed_edges:
        if gradient_ascending:
            gradient_direction = 'ascent'
            cv = 100*rho.max()
        else:
            gradient_direction = 'descent'
            cv = -100*rho.max()
        
        pad = 2
        shape_old = rho.shape
        shape_new = (shape_old[0]+2*pad, shape_old[1]+2*pad, shape_old[2]+2*pad)

        rho_new = np.full(shape_new, cv)
        rho_new[pad:-pad, pad:-pad, pad:-pad] = rho

        combined_shift_vector = np.add(combined_shift_vector, np.dot(-pad* 1.0/np.array(shape_old), cell) )

        s = np.array(shape_new)/np.array(shape_old)
        cell = cell.dot(np.array([ [s[0],   0.0,  0.0],
                                [    0.0,  s[1],  0.0],
                                [    0.0,   0.0,  s[2]]]))

        rho = rho_new

    #########
    spacing = tuple(1.0/np.array(rho.shape))

    
    ## this will get moved at somepoint to something like the following:
    #from skimage.measure.marching_cubes import marching_cubes_lewiner
    
    # Use marching cubes to obtain the surface mesh of this density grid
    scaled_verts, faces, scaled_normals, values = marching_cubes(rho, level = cut_off, spacing=spacing, gradient_direction=gradient_direction, allow_degenerate=False)
    #scaled_verts, faces, normals, values = measure.marching_cubes_lewiner(rho, level = 0.0, spacing=spacing, gradient_direction='ascent')


    verts = np.add( scaled_verts.dot(cell), combined_shift_vector)
    normals = scaled_normals.dot(cell)

    def set_correct_order(face,  verts=verts, normals=normals):
        r0 = verts[face[1]] - verts[face[0]]
        r1 = verts[face[2]] - verts[face[1]]
        
        normal = normals[face[0]] +  normals[face[1]] +  normals[face[2]] 
        normal *= 0.333333333333
        
        proj = np.dot(normal, np.cross(r0,r1))
        
        if proj < 0:
            face_out = (face[0], face[2], face[1])
        else:
            face_out = tuple(face)
        face_out = tuple(face)
        return face_out

    ############### blender part
    list_tuples_for_blender = []
    
    for i in range(faces.shape[0]):
        
        #fv = set_correct_order(faces[i])
        fv = tuple(faces[i])
        list_tuples_for_blender.append(fv)


    # Create Mesh Datablock
    mesh = bpy.data.meshes.new(name)
    print(rho.shape,'grid',faces.shape[0],'faces', verts.shape[0], 'verts')
    mesh.from_pydata(verts, [], list_tuples_for_blender)

    mesh.use_auto_smooth = True
    #print(dir(mesh))


    obj = bpy.data.objects.new(name,mesh)



    #bpy.context.scene.objects.link(obj) ## removed for blender 2.8
    bpy.context.scene.collection.objects.link(obj) # added for blender 2.8


    if material == None:
        isosurface_material = make_material(  name = 'isosurface_material',
                                        color = (0.7, 0.7, 0.7, 1.0))
        # a material being set at all makes it easier to change for all cell cylinders and edges laters
    else:
        isosurface_material = material


    if material!=None:
        obj.data.materials.append(isosurface_material)


    # smooth shading ! helps with 
    if smooth_shading:
        obj.select_set(True) # added for 2.8
        bpy.ops.object.shade_smooth()
